import java.util.ArrayList;
import java.util.Scanner;

public class Procesos extends SubProcesos
{
    ArrayList <String> Estudiantes = new ArrayList<>();
    Scanner myIn = new Scanner(System.in);
    int n;
    void numeroEstudiantes()
    {
        System.out.println("Cuantos estudiantes desea ingresar?");
        n=myIn.nextInt();
        System.out.println("");
    }
    void agregarDatos()
    {
        System.out.println("Ingrese los datos de los estudiantes a continuacion.\n");
        for(int i=0; i<n; i++)
        {
            agregar(Estudiantes);            
        }
    }
    void ordenar()
    {
        for(int i=0; i<Estudiantes.size()-8; i=i+8)
        {
            for(int j=0; j<Estudiantes.size()-8; j=j+8)
            {
                if(Estudiantes.get(j).compareToIgnoreCase(Estudiantes.get(j+8))>0)
                {
                    intercambio(Estudiantes,j);
                }
                if(Estudiantes.get(j).compareToIgnoreCase(Estudiantes.get(j+8))==0)
                {
                    if(Estudiantes.get(j+1).compareToIgnoreCase(Estudiantes.get(j+9))>0)
                    {
                        intercambio(Estudiantes,j);
                    }
                }
            }
        }
    }
    void imprimir()
    {
        for(int i=0; i<Estudiantes.size(); i=i+8)
        {
            imprimir(Estudiantes,i);
        }
        System.out.println("Hay un total de "+Estudiantes.size()/8+" Estudiantes");
    }
    void agregarEstudiante()
    {
        System.out.println("Ingrese los datos del estudiante a continuacion.\n");
        agregar(Estudiantes);
        ordenar();
    }
    void buscar(String x,int y)
    {   
        int contador=0;
        int contador2=0;
        System.out.print("\nIngrese "+x+" que desee buscar: ");
        String buscar=myIn2.nextLine();
        for(int i=0; i<Estudiantes.size()-7; i=i+8)
        {
            if(buscar.compareToIgnoreCase(Estudiantes.get(i+y))==0)
            {
                imprimir(Estudiantes,i);
                contador++;
            }
            else
            {
                contador2++;
                if(contador2==Estudiantes.size()/8)
                    System.out.println("\nNo se encontro ningun estudiante con "+x+":"+buscar);
            }
        }
        if(contador2!=Estudiantes.size()/8)//si funciona agregarlo a todos los buscar
            System.out.println("\nSe encontraron "+contador+" estudiantes con "+x+": "+buscar);
    }
    void eliminar(String x,int y)
    {
        System.out.print("\nIngrese "+x+" a eliminar: ");
        String eliminar=myIn2.nextLine();
        if(Estudiantes.contains(eliminar))
        {
            while(Estudiantes.contains(eliminar))
            {
                for(int i=0;i<Estudiantes.size();i=i+8)
                {
                    if(eliminar.compareToIgnoreCase(Estudiantes.get(i+y))==0)
                        eliminar(Estudiantes,i);
                }
            }
            System.out.println("\nTodos los estudiantes con "+x+":"+eliminar+" han sido eliminados.");
        }
        else
            System.out.println("\nNo hay ningun estudiante con "+x+": "+eliminar);       
    }
    void modificar(String x,int i,int y)
    {
        System.out.print("\nIngrese "+x+": ");
        String modificar=myIn2.nextLine();
        String aux=Estudiantes.get(i+y);
        Estudiantes.set(i+y, modificar);
		System.out.println("");
        System.out.println(x+" "+aux+" fue sustituido por "+x+" "+modificar"\n");
    }
}