public class OpcionesCriterio extends Procesos
{
    String N="El nombre";
    String A="El apellido";
    String C="La carrera universitaria";
    String D="El departamento";
    String M="El municipio";
    String DUI="El DUI";  
    String Di="La direccion";
    String E="El correo electronico";
    void buscarC()
    {
        System.out.println("Dispone de los siguientes criterios para buscar Estudiantes:\n"
                + "1.Por Nombre.\n"
                + "2.Por Apellido.\n"
                + "3.Por Carrera Universitaria.\n"
                + "4.Por Departamento.\n"
                + "5.Por Municipio.\n"
                + "6.Por DUI.\n");
        System.out.print("Ingrese el criterio que desee elegir: ");
        int op=myIn.nextInt();
            if(op==1)
            {
                buscar(N,1);
            }
            if(op==2)
            {
                buscar(A,0);
            }
            if(op==3)
            {
                buscar(C,3);
            }
            if(op==4)
            {
                buscar(D,4);
            }
            if(op==5)
            {
                buscar(M,5);
            }
            if(op==6)
            {
                buscar(DUI,2);
            }            
    }
    void eliminarC()
    {
        System.out.println("Dispone de los siguientes criterios para eliminar estudiantes:\n"
                + "1.Por Nombre.\n"
                + "2.Por Apellido.\n"
                + "3.Por Carrera Universitaria.\n"
                + "4.Por Departamento.\n"
                + "5.Por Municipio.\n"
                + "6.Por DUI.\n");
        System.out.print("Ingrese el criterio que desee elegir: ");
        int op=myIn.nextInt();
            if(op==1)
            {
                eliminar(N,1);
            }
            if(op==2)
            {
                eliminar(A,0);
            }
            if(op==3)
            {
                eliminar(C,3);
            }
            if(op==4)
            {
                eliminar(D,4);
            }
            if(op==5)
            {
                eliminar(M,5);
            }
            if(op==6)
            {
                eliminar(DUI,2);
            }    
    }
    void modificarC()
    {   int op=0;
        System.out.print("Ingrese el DUI del estudiante que desee modificar: ");
        String modificar=myIn2.nextLine();
		System.out.println("\n");
        if(Estudiantes.contains(modificar))
            for(int i=0;i<Estudiantes.size()-7;i=i+8)
            {
                if(modificar.compareToIgnoreCase(Estudiantes.get(i+2))==0)
                {
                    while(op!=8)
                    {
                        imprimir(Estudiantes,i);
                        System.out.println("Puede modificar los siguientes aspectos:\n"
                                + "1.El nombre\n"
                                + "2.El apellido\n"
                                + "3.La carrera universitaria\n"
                                + "4.El departamento\n"
                                + "5.El municipio\n"
                                + "6.La direccion\n"
                                + "7.El correo electronico\n");
                        System.out.print("Ingrese la opcion que desea elegir: ");
                        op=myIn.nextInt();
                        if(op==1)
                        {
                            modificar(N,i,1);
                        }
                        if(op==2)
                        {
                            modificar(A,i,0);
                        }
                        if(op==3)
                        {
                            modificar(C,i,3);
                        }
                        if(op==4)
                        {
                            modificar(D,i,4);
                        }
                        if(op==5)
                        {
                            modificar(M,i,5);
                        } 
                        if(op==6)
                        {
                            modificar(Di,i,6);
                        }
                        if(op==7)
                        {
                            modificar(E,i,7);
                        }
                        System.out.println("-Presione 0 si desea modificar otro aspecto.\n"
                                + "-Presione 8 si desea finalizar la edicion.\n");
                        System.out.println("\nIngrese el numero solicitado anteriormente: ");
                        op=myIn.nextInt();
                    }
                }
            }
        else
            System.out.println("No existe ningun estudiante con "+DUI+": "+modificar);
    } 
}
