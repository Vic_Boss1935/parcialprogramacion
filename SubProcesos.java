import java.util.ArrayList;
import java.util.Scanner;

public class SubProcesos 
{
    Scanner myIn2= new Scanner(System.in);
    void intercambio(ArrayList<String>Estudiantes, int j)
    {
        String auxA;
        String auxN;
        String auxDUI;
        String auxC;
        String auxD;
        String auxM;
        String auxDi;
        String auxE;
        //intercambio apellidos 
        auxA=Estudiantes.get(j);
        Estudiantes.set(j, Estudiantes.get(j+8));
        Estudiantes.set(j+8, auxA);
        //intercambio nombre
        auxN=Estudiantes.get(j+1);
        Estudiantes.set(j+1, Estudiantes.get(j+9));
        Estudiantes.set(j+9, auxN);
        //intercambio DUI
        auxDUI=Estudiantes.get(j+2);
        Estudiantes.set(j+2, Estudiantes.get(j+10));
        Estudiantes.set(j+10, auxDUI);
        //intercambio Carrera
        auxC=Estudiantes.get(j+3);
        Estudiantes.set(j+3, Estudiantes.get(j+11));
        Estudiantes.set(j+11, auxC);
        //Intercambio Departamento
        auxD=Estudiantes.get(j+4);
        Estudiantes.set(j+4, Estudiantes.get(j+12));
        Estudiantes.set(j+12, auxD);
        //Intercambio Municipio
        auxM=Estudiantes.get(j+5);
        Estudiantes.set(j+5, Estudiantes.get(j+13));
        Estudiantes.set(j+13, auxM);
        //Intercambio Direccion
        auxDi=Estudiantes.get(j+6);
        Estudiantes.set(j+6, Estudiantes.get(j+14));
        Estudiantes.set(j+14, auxDi);
        //Intercambio Correo
        auxE=Estudiantes.get(j+7);
        Estudiantes.set(j+7, Estudiantes.get(j+15));
        Estudiantes.set(j+15, auxE);
    }
    void imprimir(ArrayList<String>Estudiantes,int i)
    {
        System.out.println("Apellido: "+Estudiantes.get(i)+".\n"
        + "Nombre: "+Estudiantes.get(i+1)+".\n"
        + "DUI: "+Estudiantes.get(i+2)+".\n"
        + "Carrera Universitaria: "+Estudiantes.get(i+3)+".\n"
        + "Departamento: "+Estudiantes.get(i+4)+".\n"
        + "Municipio: "+Estudiantes.get(i+5)+".\n"
        + "Direccion: "+Estudiantes.get(i+6)+".\n"
        + "Correo Electronico: "+Estudiantes.get(i+7)+".\n");
    }
    void agregar(ArrayList<String> Estudiantes)
    {
        System.out.print("Ingrese el Apellido: ");
        String apellido=myIn2.nextLine();
        Estudiantes.add(apellido);
        System.out.print("Ingrese el Nombre: ");
        String nombre=myIn2.nextLine();
        Estudiantes.add(nombre);
        System.out.print("Ingrese el DUI: ");
        String dui=myIn2.nextLine();
        Estudiantes.add(dui);
        System.out.print("Ingrese la Carrera Universitaria: ");
        String carrera=myIn2.nextLine();
        Estudiantes.add(carrera);
        System.out.print("Ingrese el Departamento: ");
        String departamento=myIn2.nextLine();
        Estudiantes.add(departamento);
        System.out.print("Ingrese el Municipio: ");
        String municipio=myIn2.nextLine();
        Estudiantes.add(municipio);
        System.out.print("Ingrese la Direccion: ");
        String direccion=myIn2.nextLine();
        Estudiantes.add(direccion);
        System.out.print("Ingrese el Correo Electronico: ");
        String email=myIn2.nextLine();
        Estudiantes.add(email);
        System.out.println("");
    }
    void eliminar (ArrayList<String>Estudiantes,int i)
    {
        for(int j=0;j<8;j++)
            Estudiantes.remove(i);
    }
}